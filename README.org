* hesso-latextemplate-thesis

From https://github.com/mdemierre/hesso-latextemplate-thesis

A LaTeX template for the HES-SO//Master MSE thesis.

Say goodbye to ugly and proprietary MS Word files!

** CI/CD
Some files where added to be able to build this report with 
the GitLab CI/CD pipeline.

You  can use and modify the script =./build-CICD-Docker.sh= to rebuild the Docker,
but you'll probably change the name of the repository since the current one is mine.

** Features

- Title page with official layout
- Table of contents, Table of figures, Table of tables
- Abstracts (French + English)
- Nice title styles for chapter and appendices
- Chapter table of contents (minitoc)
- Bibliography
- Glossary
- Code highlighting with minted
- Nice default typography settings

** Getting started

1. Clone this repository
2. Make sure that you have the required LaTeX packages (look into =00-settings/settings_base.tex=)
3. Fill out the =00-settings/metadata.tex= with your thesis info
4. Write your thesis
5. Use the given Makefile to build the PDF
7. ???
6. Profit

** Thanks to

- Marc Demierre, for the base of this template
- Maria Sisto, for the title page
- Loïc Monney, for the section title style, captions style and font idea
- EPFL, for the basic structure
