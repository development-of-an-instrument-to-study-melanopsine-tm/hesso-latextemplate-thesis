#!/bin/bash

echo"Rebuilds the latex image and pushes it to Dockerhub"
docker build  --tag latex .
docker tag latex sunoc/latex
docker push sunoc/latex
