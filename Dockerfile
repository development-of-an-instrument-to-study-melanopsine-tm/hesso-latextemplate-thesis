FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

# Base prgramms
RUN apt-get update  && apt-get dist-upgrade -y  \
	&& apt-get install -y \
	make \
	git \
	python3-pip \
	xindy \
	gnuplot

# Needed Latex packages
RUN apt-get install -y \
	texlive-base \
	texlive-latex-extra \
	texlive-bibtex-extra \
	texlive-fonts-recommended \
	texlive-fonts-extra \
	texlive-lang-english \
	texlive-lang-french \
	texlive-latex-recommended \
	texlive-pictures \
	cm-super \
	biber \
	texlive-bibtex-extra

# Python dependances
RUN pip3 install Pygments
