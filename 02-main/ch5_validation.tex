\chapter{Validation}\label{chap:validation}

The system that was developed needs to be tested to ensure that the requirements and goals that were set in the beginning are satisfied.
This chapter presents the procedures used to validate the monitoring system and to be sure that everything works as expected.

\minitoc\newpage


% =============================================================================
\section{Single Ganzfeld primary test system}
During all the development phase, a \Gls{led} + drivers system, taken from a Ganzfeld bench was used to test the ongoing development of the monitoring system.
This system and an oscilloscope were the main validation tools during the development. It allows to make test on the actual hardware while every useful signals are readily available for measurement.

The \autoref{fig:ganzfeld_test} below shows the hardware used during the project (without the monitoring devices themselves).

\begin{figure}[H] 
  \centering 
  \includegraphics[width=0.6\columnwidth]{ganzfeld_test}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Ganzfeld test hardware}{All the components of the experiment setup used for the development of the monitoring system.
    These are the components used for a single channel of Dr.\ Tsujimura's Ganzfeld experiment.
    From top to bottom, we have: the lab $12V$ power supply, then the copper heat-sink with the \Gls{led} attached on it with its temperature sensor and the photodiode.\\
    Then from left to right, we have the \Gls{led} driver board with the test points on the right, a breadboard in the middle that was used for the first test of the filter, and finally the \texttt{STM32} based, \Gls{pwm} control board.
    By sending \Gls{json} commands to the \texttt{STM32} board from a computer, it is easy to control the brightness of the whole system by adjusting the produced \Gls{pwm} signal.\\
  Then the \autoref{fig:ganzfeld_test_wiring} after shows how these parts are connected and also the way the monitoring system is installed on it.}{own photography}\label{fig:ganzfeld_test}
\end{figure}

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1.1\columnwidth]{ganzfeld_test_wiring}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Ganzfeld test hardware wiring schematic}{A schematic with every single devices needed to operate the test system on a single channel of the Ganzfeld experiment system.
    The schematic shows the Ethernet cables (green) and the power supply cable needed.
    The orange cable is important since it is the custom part meant to connect the test points of the \Gls{led} driver to the \Gls{pwm} inputs, as previously presented in the \autoref{sub:customcable}.
  In general, this schematic shows the detail of the connection between the devices and the way they are attached. This exact situation was used during the development to validate the created solution, but also during the actual implementation of the monitoring system into the real testbench, these connections stay very similar, especially  around the \Gls{led} driver since this board comes with pre-made cable that can directly be used to connect this board and the rest of the system.}{own}\label{fig:ganzfeld_test_wiring}
\end{figure}


% =============================================================================
\newpage
\section{\Gls{json} over USB}
The program usually used in the lab to control the \Gls{led}s during the experiment is the $C\#$ Windows desktop application written by Corentin Barman in his thesis\cite{barman_four-primary_2018}.
While this software has a nice and usable \Gls{gui}, for my tests I wanted to be able to send simpler, easier to modify \Gls{json} commands.
This is why a reduced Python script (\autoref{lst:usb-serial}) was created in the beginning of the project. This script makes it easier to send simple commands to the \texttt{STM32}, like ``one \Gls{led} at 50\%'' or ``all the \Gls{led}s at maximum brightness'', while knowing exactly what was sent in the \Gls{json} package (\autoref{lst:basicjson}).

In fact, what is actually done in the script of the \autoref{lst:usb-serial} is that the base $16bits$ value for the duty cycle of the \Gls{pwm} is taken from the base \Gls{json} and then a percentage is apply. This is easier to know the sent duty cycle as the \texttt{duty} base value can be set to $65535$, then the percentage in the Python becomes directly the percentage of the duty cycle.

An issue regarding this \Gls{usb} communication will be presented later, in the \autoref{sub:serial_com_monitor}, but the communication complexity is asymmetrical here: on the computer side, it is very easy to just send messages in the serial device the control board is connected to, but on the other end, the receiving device needs some specific components and firmware to be able to receive the messages and be visible in the \Gls{usb} line as a serial device.
.

\begin{listing}[H]
  \pythonfile[firstline=3,lastline=31]{02-main/listings/usb-serial.py}
  \captionsource{Basic Python serial}{Header part of the Python script used to send basic control commands to the \texttt{STM32} board.\\
    First we have the configuration of the bus parameter itself, with the \Gls{usb} device \texttt{/dev/ttyACM0} being identified here.\\
    Then a template \Gls{json} file is read, modified (the \texttt{PERCENT} variable can be used to dim the light intensity) and then sent through the serial interface.\\
    The base \Gls{json} is visible below in the \autoref{lst:basicjson} and it is basically the most minimal structure we can use to light a \Gls{led}.}{\texttt{./python-serial/main.py}\cite{gitlab_control}}
  \label{lst:usb-serial}
\end{listing}

\begin{listing}[H]
  \jsonfile{02-main/listings/basic.json}
  \captionsource{Basic \Gls{json} for \Gls{led} control}{This is the  most basic command one can send to the \texttt{STM32} board to control a \Gls{led}: a index value (name of the \Gls{led}) and a duty cycle value (on $16bits$, from $0$ to $65535$). This structure can be read by the script presented in the \autoref{lst:usb-serial} above and is then transmitted to the \texttt{STM32} control board to drive the \Gls{led}s with the appropriate \Gls{pwm}, configured with this duty cycle value.}{\texttt{./python-serial/test.json}\cite{gitlab_control}}
  \label{lst:basicjson}
\end{listing}

% =============================================================================
\section{\texttt{pylint}: coding standard for Python}
To ensure the quality of the Python code used in the monitoring system, \texttt{pylint}\cite{pylint_analyser_web} was used.

This quality control and coding standard analysis tool was useful on three elements:
\begin{itemize}
\item Keeping a uniformity in the code formating must be regarded.
  pylint has a very strict guideline on how it want you Python code to be formatted. This allows to have a consistent looking, more readable code across all of you files. As visible in the \autoref{fig:lint_line} below, the rules for formating are strong and really ensure that all the parts of the code look sound.
\item Suggesting of using more Python good programming practices.
  the tool is also capable to show and give advice about programming patterns and habits.
  For instance, pylint tells you if your class only contains a single method. Which make you think about it and maybe restructure your code for it to make more sens.
\item Forcing you to be consistent and have docstring for every single method in the code.
  If the method are used somewhere else, some command like \texttt{print my\_function.\_\_doc\_\_} will display information about the method / function. Also these docstring acts as a clearly visible block of comment when the code is read. Such docstring can be seen in the \autoref{lst:adc} in the previous chapter.
\end{itemize}

The Python code written for this projects monitoring system is guarantee to be rated 10/10 by \texttt{pylint}. This was one of the advantage of using \texttt{git} as a version control system: since my lint tool does not work when editing code remotely on the Raspberry Pi (through \texttt{SSH}), it was easier to just push the modifications from the Raspberry Pi, pull them on my main computer and make the changes to stay lint compliant there.
The \autoref{fig:lint_line} below shows the kind of messages or information this \texttt{pylint} tool can give to ensure the uniformity of the code.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{lint_line}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Lint code verification at work}{The Lint verification tools forces the formating down to the number of blank lines between methods.
    Even if these element does not compromise the actual execution of the code, they reach for a better uniformity in the way the source look, making it easier to eventually modify it.\\
    In order to have the maximum rating with \texttt{pylint}, one must reach to have none of these message.\\
    The result of that can be seen in the whole \Gls{adc} Python script visible in the \texttt{./acquisition/adc.py} file in the main GitLab repository\cite{gitlab_monitoring}. Also regarding the multi-branches setup discussed in the \autoref{sec:scalecode} previously, the same script in the two other branches was also checked using the same tool.\\
  A view of the return of the \texttt{pylint} command can be seen in the \autoref{fig:pylint} below.}{own screenshot}\label{fig:lint_line}
\end{figure}

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{pylint}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Example of \texttt{pylint} return}{Here is a view of the return of the \texttt{pylint} command (when not integrated in a text editor, like Emacs for the \autoref{fig:lint_line} above). For the examples here, a docstring was remove to show the type of error message that is shown and its impact on the code grading.}{own screenshot}\label{fig:pylint}
\end{figure}


% =============================================================================
\newpage
\section{Redeployment on a clean system}
This is the main reason to be and strength of containers used for this project: because they contain all the dependencies and they have a precise description on how they should be deploy with the \texttt{docker-compose.yml}, a relevant test is to format the host system and try to redeploy everything from there to ensure that nothing was forgotten.

This is also a benchmark for the wiki\cite{gitlab_monitoring_wiki} and the documentation of the project in general, including the present report: by redoing the base setup from the beginning, it is possible to tell is some information is missing.

It turns out that this process of redeployment was made when the setup of the monitoring system needed to be made for the LEDCube. In its current form, the system is simpler: only one type of sensor and no need for an extra \Gls{pwm} filter board to be crafted.
Yet it was a good test to see how easy it is to redeploy a system from scratch and configure it. The \autoref{fig:raspi_cube} below shows the \Gls{rpi} used for the LEDCube, with only the shield and the jumper wires from the photodiodes coming from the test bench.

The deployment of this LEDCube monitoring was as painless as expected and for the current situation, the part that takes the most time is to burn the base \Gls{os} on the SD card and then ask to the university IT department to whitelist the new device IP for it to grant access to the internet.
All the rest can be done with only a couple of commands and in a few minutes, the system is ready to be configured in the Node-Red graphical interface and used!

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{raspi_cube}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{The LEDCube Raspberry Pi}{A picture of the Raspberry Pi 4 used as the monitoring device for the LEDCube and as the redeployment from scratch test device. There is no need for the second piggy-back board there since no \Gls{pwm} signal is measured. Instead we can see the jumper connectors from the LEDCube photodiodes (\autoref{fig:cube_photodiode}) arriving on the \Gls{adc} inputs.}{own photography}\label{fig:raspi_cube}
\end{figure}

% =============================================================================
\newpage
\section{Usage of the monitoring system on an existing bench}
As a final step for the project, the assembly of the monitoring system on the existing Ganzfeld bench in the laboratory is meant to be the last validation for the functionalities implemented during this project. The \autoref{fig:ganzfeld_started} below shows the running bench with the two monitoring Raspberry Pis mounted on it.

The Node-Red interface meant to monitor four Ganzfeld primaries at once was already created, as visible in the \autoref{fig:ganzfeld_monitor}, so the installation and start of the system was fast and no issues occurred. The devices were able to monitor all the required signal, including the four going through the low-pass filter.
Now for the system to be more accurate and useful for the actual measurement and monitoring of an experiment, some more calibration might be required, as suggested later in the \autoref{sub:calibrationfunc}.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{ganzfeld_started}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{A working Ganzfeld bench with Raspberry Pi devices on it}{Similar to the \autoref{fig:ganzfeld_bench} shown earlier, this image shows a working Ganzfeld bench with the two monitoring devices connected on it.\\
  We can see the custom cables (\autoref{sub:customcable}) connecting two drives boards to their dedicated Raspberry Pis.}{own photography}\label{fig:ganzfeld_started}
\end{figure}


% =============================================================================
\newpage
\section{Conclusion}
% the single primary system for dev
During this project, the continuous testing and validation of the proposed solution eventually lead to the current stable and properly formatted software system. By having an actual device, identical to the one used on the test bench, it was easier to actually understand the needs and specificity of the test benches during the development. And also in the end of the project, this setup was useful to validate that was done and ensure that the values that are monitored makes sens with the behavior of the \Gls{led} control system.

% pylint
With the widely available and easy to use \texttt{pylint} tool, it was convenient to apply the \texttt{lint} principle to the Python, to try and reach for a more sound formatting and better coding practices in general.
As the required changes were displayed right away in my text editor, this quality insurance could be kept during all the development phase.
Following good programming practices and a strict formating can be seen as a constraint, but it actually help to implement consistent and sensible functionalities while having a clearer view on what is happening around.
Furthermore, it is useful for the next person that will eventually look inside and modify this Python script.

% redeployment and setup
Finally and because of the modular nature of the proposed software solution, a complete reset and recreation of the system was a good way to test and see if everything works as intended. This was done on the deployment of the monitoring system on the LEDCube bench, since this operation was made near the end of the project.
