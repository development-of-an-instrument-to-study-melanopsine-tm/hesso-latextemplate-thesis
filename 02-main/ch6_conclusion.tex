\chapter{Conclusions}\label{chap:conclusions}
\minitoc\newpage


% =============================================================================
\section{Project summary}
The goal of this Master thesis project is to develop a monitoring system for scientific test-benches involving \Gls{pwm} controlled high power \Gls{led}s.

Working at the Nagoya City University for the Dr.\ Sei-ichi Tsujimura, this project deals with the uses of testing devices to study psycophysical reactions regarding the eye, especially on the topic of the melanopsin: a protein produced by light excitation, but not involved in the vision. This can be done by having a light source that uses at least four primaries, keeping cones silent while exciting melanopsin with methamers. More details about these two concepts in the \autoref{sub:goal}.

The current test system for these are using a microcontroller base board using a \texttt{STM32} chip to pilot a driver board, which will ultimately power the light sources.\\
With this project, the target is to provide a monitoring solution to have a control view  over the test systems benches used in the laboratory.\\
By using external devices to monitor the outputs and the variables of the system and to display them live to the scientist conducting the experiment, some environmental variations can be detected which should lead to more precise, better quality experiments.

In order to accomplish this task, a IoT-style, Raspberry Pi-based, container-powered system was put into place. When using Docker to separate the part of the application from one another, it is easier to scale it and add more devices, as it was required to be able to completely monitor the more complex Ganzfeld bench  that outputs multiple signal for each of its four light sources.
By having the data reading program, the user graphical interface, the raw measurement storage system and the container manager placed with all their dependencies in separated instances, the software solution is easy to deploy, scale and distribute.\\
On the hardware side, the Raspberry Pis were topped with a commercial multi-channel, analog-to-digital shield, and also a two-channel, home-made, hand-soldered low-pass filter second board is the measurement of a \Gls{pwm} signal is required.


% =============================================================================
\newpage
\section{Comparison with the initial objectives}
\subsection{Add new features on the testing systems}
Adding the  monitoring system to the test benches provided many new possibilities for the testers. As the project was advancing, some choices needed to be made on the features to implement and what needed to be left for later or someone else (see the \autoref{sec:futur} below about this topic).

Here is what is possible to do with the current system:
\begin{itemize}
\item Monitor the \Gls{pwm} duty cycle, current and temperature for the Ganzfeld \Gls{led} driver board.
\item Monitor the \Gls{led} brightness of the Ganzfeld.
\item Monitor the \Gls{led} brightness for multiple points in the LEDCube.
\item Having a live view for each bench.
\item Having a \Gls{csv} file for each experiment on each bench.
\item Controlling and viewing the state of the monitoring software to each bench.
\end{itemize}


% -----------------------------------------------------------------------------
\subsection{Improve the quality of the scientific experiments}
This objective is harder to quantify, but there are some identifiable factors that can be detected with this monitor system that could lead to some better quality scientific measurements.

For instance, it was visible that the temperature of the \Gls{led} was changing beyond the noise level because of some external factor (air conditioner starts, door opens). No only these changes were measurable but it was visible that they have an impact on the brightness of the tested \Gls{led}.

By taking such factor into account, it should be possible to reduce the statistical variation of the future measurements. Using the measured variations of brightness and / or temperature as a normalisation factor for the measurement will reduce the uncertainty of such variation being an actual and significant result or just a variation in the testing environment conditions.

% -----------------------------------------------------------------------------
\newpage
\subsection{End-user documentation, usage \& modification accessibility}
The part where some documentation was written for non-engineer students to be able to used, modify and debug my system was a challenge for the final part of this project. Here are the key components toward this accessibility goal:

\begin{itemize}
\item This report. Containing all the details, reasons and appendix to have the full details of this thesis project, the current document is a precious tool if some specific details need to be find, yet because of its extensive amount of pages, it is definitely not the most convenient handbook for a everyday use of the system developed here.
\item The wiki page\cite{gitlab_monitoring_wiki}. This is the response to the issue of the previous point: to allow the end users of the system to understand it and eventually as a guide to redeploy / recreate it, this wiki page aim to be a step-by-step, lighter guide with some information on the reason of some implementation choices, but without all the complexity of the inner working of the system.
\item The \texttt{Dockerfiles} and \texttt{docker-compose}.yml (visible in \autoref{chap:docker-compose}). Now if some information about the inside of the system is needed but hard to find in the thesis, looking at the \texttt{docker-compose}.yml file could be a good idea. It contains a readable description of all the part of the software system and shows the external parameters of each services.
\end{itemize}


% =============================================================================
\section{Encountered difficulties}
Here are some elements that were more challenging or that caused issues during the development of this project.

% -----------------------------------------------------------------------------
\subsection{2020 pandemic}\label{subsec:pandemic}
This is obviously the major disturbance event that occurred during these 6 months of project. After only a couple of days into the start of the project, the state of emergency was declared in Japan\cite{japantimes_covid_2020} for many major cities, including Nagoya. From then, no students were allowed to come to the university.

Amid this still ongoing worldwide pandemic and because of the lock-down in Japan from April to June, a complementary job was proposed to unsure the continuation of the project while being unable to physically come to the laboratory to work on the main system.
This is the second part of the project: the writing of the article about the other studies devices\cite{conus_instrumentation_2020}, as discussed in the \autoref{sec:other_inst}.

Because of this context and the fact that the project will focus on devices used for subject I am less familiar with (psycophysics \& light), it appeared that a good use of the time before being able to work again on the main system would be to understand what are the needs and solutions of other systems, used for similar experiments.

Mr.\ Martial Geiser proposed a general review of the devices using the silent substitution method, aiming to be published in peer-reviewed journals.


% -----------------------------------------------------------------------------
\newpage
\subsection{Measurement precision \& noise}
The available precision provided by the monitoring system was a concern of Dr.\ Tsujimura from the start of the project.

As presented in the \autoref{subsub:extension shield}, the noise problem for this project was on the \Gls{adc} reading. When the maximum speed of reading is needed, a lot of noise appears. This can eventually be mitigated with filtering, yet a longer job of analysis, simulation, test and error will be needed to reach a higher precision and quality of the input signal.


% -----------------------------------------------------------------------------
\subsection{Serial communication monitoring}\label{sub:serial_com_monitor}
When it was time to chose what signal to monitor in the \Gls{led} control system, reading the \Gls{json} from the PC to the \texttt{STM32} board looked to be a useful and easy task.

Except it was not: at first, the communication was thought like a \Gls{i2c} line where I could simple add a new slave on the line and listen to the master commands.
However, it appears that the communication used by the PC is a \Gls{usb} specific serial protocol: \Gls{usb} \Gls{cdc} what require some extra hardware to be decoded.
This the current devices available (another dedicated piece of hardware would have been needed between the USB line and the Raspberry Pi) and regarding the lesser importance of having a direct confirmation of the sent \Gls{json} packages, this part of the monitoring was discontinued.


% =============================================================================
\section{Solutions explored but unused}
During the design and early implementation of the system, some solutions or design choices were tried or considered in detail but eventually stayed unused.

% -----------------------------------------------------------------------------
\subsection{Replacing the \texttt{STM32} control board}\label{sub:replace1}
This was taken into consideration in the early stages of the project. Because of the difficulty to use and modify the firmware of this board (especially for the students using the benches), starting from scratch with something else was an option.

However, these \texttt{STM32} systems are industry standards and the current system works (''If it ain't broke, don't fix it'').
But this was still a useful discussion and the actual starting point of the development of the monitoring system: if it is possible to validate the way the current system work with an external device, it might be easier to replace this board by something else in the future and ensuring that the testing system will operate the same.

% -----------------------------------------------------------------------------
\newpage
\subsection{\Gls{tdd} paradigm}\label{sub:tdd}
After the prototype version of the Python script was tested, it was time to choose how to implement the cleaner version of this system.
Being interested in that topic and while I wanted to redo the Python program from the ground up, the \Gls{tdd} methodology was considered.

The idea of this system is to first write tests for the program and then write the actual code that should comply to these tests.
However in the current situation, this was difficult to apply because a big part of the program relies on reading hardware (\Gls{i2c}) and work with the result.  It turned out that this is a hard task and after some trial, this methodology was abandoned.

Some more classic \Gls{oop} paradigm was chosen from there, as presented in the \autoref{subsub:oop}.

% -----------------------------------------------------------------------------
\subsection{Swarm / continuous deployment}
With the whole system being Dockerized and as presented in \autoref{sub:autodeploy}, can be deployed all at once thanks to the \texttt{docker-compose} technology, it was tempting to go one step further and try and make the system to be able to deploy itself automatically using technologies like \texttt{docker swarm} or \texttt{Kubernetes}, as well as the \gls{cicd} tool provided by GitLab and already in use for this thesis report (\autoref{sub:latexcicd}).

This is a general dilemma in software development: making a system more flexible, automatic and generic and will make it bigger, bloated and more complex, while keeping the code minimal will reduce the risk of errors but makes the system less flexible or portable.

In this project, the use of container seems reasonable because it allows to have a clear context for each application and it allows to redeploy everything easily with a single script, but having a management system for the whole system is maybe a step too far: we do not have a whole swarm of IoT objects here and re-deploying each of the devices when a modification is made they are is actually a simple task.



% =============================================================================
\newpage
\section{Future perspectives}\label{sec:futur}
This part will present the ideas of improvement that can be done in the future to continue the work on the current system.

% -----------------------------------------------------------------------------
\subsection{Mechanical assembly, re-calibration \& setup}\label{sub:newmec}
This one is an unfinished task, due to the late arrival of some mechanical and electrical parts: the monitoring devices are currently wired and where tested on the benches, as visible previously in the \autoref{fig:raspi_cube} and \autoref{fig:ganzfeld_started}, but the definitive brackets to hold the Raspberry Pis to the structure of the Ganzfeld bench were not received yet, as well as some power-jack cables.
  
% -----------------------------------------------------------------------------
\subsection{Calibration \& custom conversion functions}\label{sub:calibrationfunc}
During the test and validation of the system, the conversion made from the read voltage value from the \Gls{adc} to a value that represent a reality (a temperature, for example) was empirical and not a lot of time was spent doing that.
The goal at the moment was to produce value that makes sens, to ensure that the system is working, but no precise calibration of the inputs was made.

% -----------------------------------------------------------------------------
\subsection{Remote experiment control \& log}
Even with the current system being used through the network for the monitoring, it is still required to connect the experiment PC to the device with a \gls{usb} cable to control the experiment. The experiment PC runs the C\# software that gives the control values to the \texttt{STM32} board.

An significant improvement that can be done would be to connect the \texttt{STM32} directly to the Raspberry Pi and then, using an interface between the C\# program and the Raspberry Pi, control the experiment remotely.

This would also be useful since it allows to monitor more easily whats is happening inside the control software, even though some modification would  be required to extract the information. An element that Dr.\ Tsujimura propose was to be able to log the experiment condition. With the devices on the bench, it is currently not possible, as explained in the \autoref{sub:serial_com_monitor}, but if the control software was modified and was able to send information i.e.~on the Samba server, it become easy to just log the produced \Gls{json} to have a trace of what the experimenter wanted, versus what the measures (current system) show.

% -----------------------------------------------------------------------------
\newpage
\subsection{Cloud monitoring interface}
Because of the self-hosted nature of the system and the fact that it is running inside a school network, nothing from this monitoring interface is accessible from the outside.

The \autoref{fig:cloudbased} below shows how some more Node-Red can be used to provide some information on the system to the outside world.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=0.9\columnwidth]{cloudbased}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Network structure with cloud elements}{Similar to the \autoref{fig:scope_channels_net}, this schematic shows how the monitoring architecture could be extended with some elements in the cloud. The idea here is to have a second ``layer'' of Node-Red that is hosted on a cloud \gls{iaas} provider (\gls{aws}, \gls{gcp}, etc.).\\
    The systems inside the local network can provide the information within this scope and also send some data to this new graphical interface, making a part of the data available from the outside.}{own}\label{fig:cloudbased}
\end{figure}

% -----------------------------------------------------------------------------
\newpage
\subsection{Delay for multi-devices measurements}\label{sub:delaymulti}
When we have the situation of the Ganzfeld bench, where two separated devices are measuring primaries for the same experiment, the question of the coherence and the delay between the measurement can be asked.

Some deeper analysis and improvement can possibly be done in that direction.

% -----------------------------------------------------------------------------
\subsection{About the measurement of the spectrum}\label{sub:spectrum}
This is an important data currently missing from the monitoring system: having a live view of the spectrum of the produced light.

Measuring a light spectrum is no easy task and require some specific equipment. This task has not been done during the project, but it could be interesting to have a dedicated device that measure the stimulus spectrum at any time and that sends the values to the Node-Red to be monitored.

This measurement, in relation with all the other can be an important way to control the precision of the test system while the spectrum will inevitably vary with temperature changes, tension variations or others.
% -----------------------------------------------------------------------------
\subsection{About the \Gls{pwm} filter \& the \Gls{adc} shield noise levels}\label{sub:filternoiselevels}
The noise level is currently limiting the precision of the measurement of the \Gls{pwm} duty cycle. As shown in the \autoref{subsub:extension shield} with some measurement using the \Gls{adc} shield, the noise level in the input of the system is important and thus reduces the quality of the gathered data.

This situation could be mitigated in two way:
\begin{itemize}
\item Using a higher quality \Gls{adc}. This is the easy, yet expensive way to improve the precision and speed of the measurement. With some audio-quality \Gls{adc} device being used instead of the current, more \gls{diy} shield, a gain in the monitoring quality could be archived.
  However, such devices are often way more expensive and have less separated channels (usually only one or two). This mean that such upgrade should be done only for a particular signal that is critical and that actually needs to be monitored more precisely.
\item Adding another layer of filtering / amplification before the \Gls{adc}. Currently, a low-pass filter is used to obtain a \gls{dc} values for the \Gls{pwm} signal. With its small gain (1.1), the noise amplification is not very important, but it still can be significant when a very small duty cycle tries to be measured. This was presented in the \autoref{subsub:precision_noise_level}: when the duty cycle goes below \texttt{5\textperthousand}, the \Gls{snr} reaches about 1.
  By adding some more filtering, it might be possible to reduce said noise and thus increase both the minimum duty cycle value measurable and also the maximum precision of all the other \Gls{adc} inputs.
\end{itemize}

% -----------------------------------------------------------------------------
\subsection{About the NodeRed frontend}
The NodeRed interface offers many tools to design an interface and manage the data received. Some improvements can be done there. The \autoref{fig:ganzfeld_monitor} seen before shows the current interface for the Ganzfeld system, but every things about it can be changed as the interface visible in the \autoref{fig:flow} is readily accessible to the end user.

% -----------------------------------------------------------------------------
\subsection{Replacing the \texttt{STM32} control board}
This subsection actually has the same name as the \autoref{sub:replace1} above, but the purpose here is slightly different. At first I thought of directly replacing the \texttt{STM32} control board, but now the monitoring system is in place the replacement could be done in another way:

\begin{enumerate}
\item Using the monitoring system to determine a set of tests to ensure the control board is working correctly.
\item Setting the new, replacement board with the similar functionalities.
\item Submit the same set of tests to the new board and determine if it is functionally close enough to the \texttt{STM32} to replace it.
\item Redo regularly this base test to the new board as new features are added.
\end{enumerate}

With this situation, instead of making a simple replacement of the current system, we are doing some sort of \Gls{tdd} (as presented for the software above, in the \autoref{sub:tdd}) for the control system: this project developed a monitoring system that shows the behavior of the bench when some control commands are sent. The next step is to write the tests based on these measurement. These tests can ensure the working condition of the current control board. Then the new one can be programmed and configure first to satisfy the tests.


% =============================================================================
\newpage
\section{Personal comments \& conclusion}
While this was not my first abroad thesis project, it is always a pleasure and a great experience to be allowed to study, work and meet new  people in a completely different environment. Even with the unexpected worldwide pandemic of these last month, the project could advance smoothly in general: a goal was find in the early days and Dr.\ Tsujimura was available to discuss the evolution of the project and the goals we were trying to archive.

Especially in this context where I was in charge of a complete system during this project, it was very pleasant to be able to choose and work on topics I had a strong interest in, like containers or IoT.

Even though this whole project was a separated entity compare to the rest of the work done in the laboratory, all the people here were friendly, willing to help and opened to my proposed technological solutions. I hope that the work I provided during these 6 months will be helpful here and improve the quality of the working environment for all the person that need to operate these testing system that are now equipped with my monitoring system.\\

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{nagoya}
  % {TOC entry}{Actual caption}{Source}
  %\captionsource{Network structure with cloud elements}{Similar to the \autoref{fig:scope_channels_net}, this schematic shows how the monitoring architecture could be extended with some elements in the cloud. The idea here is to have a second ``layer'' of Node-Red that is hosted on a cloud IaaS platform. The systems inside the local network can provide the information within this scope and also send some data to this new graphical interface, making these data available from the outside.}{own}\label{fig:cloudbased}
\end{figure}