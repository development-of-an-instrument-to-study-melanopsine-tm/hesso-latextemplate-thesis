\chapter{Analysis}\label{chap:analysis}

This chapter presents the gathering of information on the existing devices used to study melanopsin at Dr.\ Tsujimura's laboratory. The analysis focuses on the solutions available to improved these systems.
The search and comparison of the other melanopsin test devices used in peers-reviewed articles was done during the lock-down and will be discussed here.

\minitoc\newpage


% -----------------------------------------------------------------------------
\section{Current systems}
The systems currently available at the laboratory for the study of melanopsin are functional for the experiments and can be used by the students to perform their research. There is two main systems used here, for different purpose with separated hardware (not the same \Gls{led}s or \Gls{led}s driver board), but the control board that produces the driving signal for the \Gls{led}s is the same on both systems. This also mean that the two experiments can be controlled with the same software. Only the configuration of said software needs to be adapted for the system to produce the required wavelength.


\subsection{LEDCube experiment}\label{sub:ledcube}
The LEDCube is a box with a large light diffuser (300mm x 300mm), similar to a photography soft-box, but with the possibility to use a large number of singular light sources (primaries) producing a defined wavelength: currently 16 of them are used, up to 20 can be soldered onto the current board. Each primary is composed of 4 \Gls{led}s that are controlled together by the same channel on driver board.

This device is used for room-size illumination test, focusing on the circadian cycle of the test subject: because of the softer and more spaded light, one can use this as a ceiling lamp and keep it there for hours or days when still being able to control the spectrum of the system with all the primaries available. The \autoref{fig:cube} shows the looks of this system and a potential usage context. The \autoref{fig:cube_leds} after that shows the 4 arrays of \Gls{led} meant to provide more illumination uniformity.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{cube}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{3D view and context of the LEDCube}{A 3D design view and a possible experiment context for the LEDCube system.
    We see here the possible usage of multiple LEDCubes to reach a light uniformity in the room.
    Yet the device does not support any kind of chaining so each LEDCube need to be setup individually.
    As it is visible on the picture, two fans are used in the device to cool the \Gls{led}s and try to reduce they change in light intensity or spectrum shift due to their produced heat. This means that some noise is created when in operation.}{Corentin Barman 2018 Bachelor thesis\cite{barman_four-primary_2018}}\label{fig:cube}
\end{figure}

\begin{figure}[H] 
  \centering 
  \includegraphics[width=0.8\columnwidth]{cube_leds}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{LEDCube \Gls{led} arrays}{Below the translucent lid are these 4 \Gls{led}s arrays, each of them having 16 independently controllable \Gls{led}s that will act as separated primaries to do the silent substitution.
    The maximal number of individual primaries usable with this hardware is 20, as stated before. Currently, the 16 primaries are the \Gls{led}s in the $4\times4$ squares, but 4 more \Gls{led}s can be added using the soldering pads, adding another column of primaries for each array.\\
    By having these 4 \Gls{led}s for each primary light, we try to provide more uniformity: as shown on the \autoref{fig:cube} above, the LEDCube has a semi-translucent lid that diffuses the light.
    Now instead of having a single point that is diffused, we have four of them for a better spreading of the light.\\
  Yet the uniformity of the lightening is still a concern of Dr.\ Tsujimura for this devices and a solution to verify this should  be provided.}{own photography}\label{fig:cube_leds}
\end{figure}

\newpage
\subsection{Ganzfeld experiment}\label{sub:ganzfeld}
The Ganzfeld is a second bench that uses high power \Gls{led}s to bring light to the eye of the test subject. This system is intended for a completely different usage compare to the LEDCube. Four primaries are used here and will diffuse the light directly at the face of the test subject using optical fibers and eventually other light diffusing devices, such as the integration sphere presented in this paper of Dr.\ Tsujimura\cite{tsujimura_contribution_2010}. This means that the subject must be staying at the test bench for the whole experiment duration without any possibility to move its head.
Thus this experiment target is to measure short-term eye variation when light context changes, such as retinal movements. See \autoref{fig:ganzfeld_bench} below for a view of this system and details on the current hardware in use for this device.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1.1\columnwidth]{ganzfeld_bench}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Ganzfeld experiment test bench}{A view of the Ganzfeld test system.\\
    On the left picture is the control electronic part with all four driver boards that power the \Gls{led}s that are mounted on their copper heat sink and connected to their optical fibers on the right of this picture.
    All the way to the left is the desktop computer power supply used to bring electricity to the whole system. Below and above the \Gls{led} drivers are the two Raspberry Pi used as the monitoring device for the whole system (final result of this current thesis project).\\
    On the right picture is the place where the subject stays to have its eye illuminated. We can see the support for the head in the center and the arrival point of the optical fibers, coming from the \Gls{led}s.}{own photographs}\label{fig:ganzfeld_bench}
\end{figure}

\newpage
\subsection{STM32 control board}\label{sub:stm32}
This is the common control board for both the LEDCube and the Ganzfeld system.
It was the first subject that was worked on when the project started.
The exact model the systems are using is the \texttt{STM32F407G-DISC1}\cite{st_stm32discovery_web} demonstration board: a educational-focused board based on the \texttt{STM32F4} chip. This device is was programmed by C. Barman during his thesis\cite{barman_four-primary_2018} to produce a \Gls{pwm} signal when the appropriate \Gls{json} input is received. The \autoref{fig:stm32disco} below shows a view of this board.

After spending some days working with it, checking the code and controlling \Gls{led}s, here is what I noticed about the usage of this board and the way it operates:
\begin{itemize}
\item The code for the current firmware is not readily available and poorly managed: the code is only stored locally on a laptop from the lab and everything is packed into a dedicated \Gls{ide}\cite{st_allolic_web} project, which makes it impossible to modify or use the code without that tool.\\
  As on August 2020, said \Gls{ide} even appears to be deprecated for new projects and ST seems to recommend moving to their new \Gls{ide}.
\item Even though the functionality of the board is not conceptually complex, the project needed to make it work is massive and intricated.
  The software project contains everything for the board to operate, from the ground up. There is basically no abstraction or clear separation between the internal functionalities (i.e. \Gls{pwm} pins definition) and the operational functionalities (i.e. \Gls{json} interpretation).
\item On a similar topic, and still as described in C. Barman thesis\cite{barman_four-primary_2018}, some other tools were used to generate the pinout configuration and maybe some other parts of the firmware. The problem is the same as above: because of the lack of a standard code management system and the use of deprecated and proprietary tools, it is very inconvenient to understand / verity / modify / improve the internal firmware of this \texttt{STM32} control board.
\item For the positive part, the \texttt{STM32} board and its chip are very common, which means that some internal, generated parts of the code are often documented or some information exists on these for similar boards.\\
\item The chip runs using a \Gls{rtos}, which means that if some real-time specification was required (maximum delay, precise task scheduling, etc.), it is possible to do it with this system. However and regarding the described situation above, this task would probably require a complete refactoring of the firmware.
\end{itemize}

\begin{figure}[H] 
  \centering 
  \includegraphics[width=0.7\columnwidth]{stm32disco}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{\texttt{STM32F407G-DISC1} board}{A view of the \texttt{STM32F407G} Discovery board. On the left and the right of the image, the \Gls{usb} ports used for power and serial communication are visible. The strong point of this board is the impressive number of outputs that can be used as \Gls{pwm} (about $50$ of them).\\
  This board is used to control the Ganzfeld experiment as well as the LEDCube according to the \Gls{json} configuration received. This  means that the end used must remember or at least pay attention to what channel of the devices they want to control and what is the associated light wavelengths. }{STMicroelecronics website\cite{st_stm32discovery_web}}\label{fig:stm32disco}
\end{figure}

\newpage
\subsection{Experiment control software}
Developed in 2018 by C. Barman for this Bachelor thesis\cite{barman_four-primary_2018}, this C\# application runs on a computer next to the experiment benches and sends to the \texttt{STM32} control board the instruction they need to operate the experiment. It allows the user to create scenarios and sequences in the variation of the lightening of a system and then send the generated \Gls{json} file to the control board.

As it is now, the students are used to operate the experiment using this program, and it was not the purpose of this project to modify this software. The only minor work that was done on this control software was to try and recompile it with a free version of Visual Studio. Everything else about it will remain untouched. But since this software was used from time to time during this project to test the monitoring system, some words about it needed to be said in this report. The \autoref{fig:controlapp} below shows a view of this application.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{controlapp}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Control application for the LEDCube}{A view from the C\# software written by Corentin Barman to control the LEDCube but also in general, any system using the \texttt{STM32} control board that transforms a \Gls{json} sequence into a \Gls{pwm} presented in the \autoref{sub:stm32} above. Even though the interface is not $100\%$ made for it, this interface can easily control the Ganzfeld bench.\\
    The software allows for direct primaries control, as visible on the picture, but also time sequence configurations where the intensity for each primary can be adjusted for the period of the test. Also configurations can be saved / imported / exported.}{Corentin Barman 2018 Bachelor thesis\cite{barman_four-primary_2018}}\label{fig:controlapp}
\end{figure}

% -----------------------------------------------------------------------------
%\section{Calibration}
%With the details presented in the previous sections and according to what was explained by Dr.\ Tsujimura, the calibration of the test devices is a long and complex procedure. Barman's software interface\cite{barman_four-primary_2018} works well to control the channels of the devices, but during the calibration, each primary will be individually controlled and have they wavelength and intensity measured.


% -----------------------------------------------------------------------------
\newpage
\section{Other instrumentation used for melanopsin excitation}\label{sec:other_inst}
During the lock-down and while the laboratory was not accessible, an analysis was made on the other devices and method used to excite and measure the melanopsin.

This part of the project materialized as an article written in collaboration with Mr.\ Geiser and eventually reviewed by Mr.\ Tsujimura.\cite{conus_instrumentation_2020}. The article is intended to be published in the MDPI\cite{mdpi_web} journal, but this has not been done yet (as of \today).
It is also important to note that the version included in this report in the \autoref{chap:instrumentation_article} is still a work in progress version of the document, not the definitive version of the paper. Some modification will eventually be done after the \today.

Here is how the comparison and analysis of the various papers we picked was done: all the articles reviewed during this part of the project were gathered in a spreadsheet, first to compare the proprieties and characteristics of the experiment (what subject, how many primaries, what wavelength, etc.), then to compare the devices used and their characteristics (custom or commercial solution, controller, display frequency, etc.). The \autoref{fig:devicetable} below shows a sample of said spreadsheet, where the devices of some articles are listed.

The next points list what was found during this research / analysis part of the project:
\begin{itemize}
\item They are not many articles that studied the specific field of research of melanopsine. While searching for paper that used the silent substitution method to study melanopsine and by adding some other relevant papers cited by others, we could only have about $30$ individual papers.
\item While some article cite a commercial solution for they illumination system, many other gave little to no details on the system used in the study to illuminate the subject eye. This makes sens since the purpose of the articles is to present the result of an optics / psycophysics study, not the engineer development of a device. Yet it seems critical to try and have a good description of the used device, since the characteristic of the \Gls{led} used or the frequency produced will have an impact on the experiment. If these are not document properly, the experiment cannot be reproduced.
\item The precision required for the produced light greatly varies between the experiment. Usually, the speed is not an issue: since we are dealing with biological systems and except for some very specific situations, changes faster than $1/100 s$ are seamless to the subject while being orders of magnitude slower that what even some basic electronic can do.\\
  The challenging part can be in the light spectrum precision. Some papers were using filters in front of a light bubble, or some commercial device. But for the one with custom electronics that uses silent substitution (\autoref{subsub:silent}), the bitwise precision of the system that controls each primary can become a limitation.
\end{itemize}

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1.1\columnwidth]{devicetable}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{List of devices in other papers}{Here is a table with the $18$ detailed devices that were found in the reviewed articles.
    Some of these are using the same device as a previous publications, thus the important and relevant devices are the first 10.\\
    We can see on the right that the devices were sorted according to their type. What was most interesting for the project were the custom electronics, since this is the type of system used by Dr.\ Tsujimura in his laboratory, but having the other one, like the projectors, can show some other needed characteristics (images / second, bitwise  color precision).\\
  For the custom electronic solution, sometimes information about the usage of a commercially available board (like Arduino) was given. These were especially interesting for us since it become possible to find the exact maximum of the system, down to the individual bitwise resolution of light intensity of each primaries.}{own spreadsheet\cite{conus_review_spreadsheet_web}}\label{fig:devicetable}
\end{figure}

For the comparison of the articles, many elements were taken into account. This includes the number of primaries, the type of subject (humans or mice), the used light frequencies, and the type of illuminations; but also some other characteristics meant for eventual later analysis, like the impact factor of the journal in which the article was published or the unit used for the measuring of the light intensity.
The fist aimed to see if a correlation can be find between the amount of information and details about the testing device hardware and the reputation of the publishing journal. The second characteristic was to be able to make some detailed comparison between the actual tests. It also shows that some information was lacking on some experiment for them to be reproduced, like the area of the illuminated area.

% -----------------------------------------------------------------------------
\newpage
\section{Conclusion}
\subsection{About the monitoring solution}
With the current experiment devices, we have two main system with a distinctive lack of monitoring to ensure the quality and fidelity of the signal produced. A single board, \texttt{STM32}-based is used to control both systems, but the use of such board makes it hard to modify and debug for non-electrical engineer students.
See the \autoref{fig:general} below for a general view of this system.
However the functions of the board should be kept: it seems to be a good option to have a complete view over the system operation to eventually be able to replace the control board later on.

The control software that runs on an external computer and that is currently used to drive the experiment shall not be modified yet, even though its replacement by some web-based, platform agnostic solution could be a useful and interesting project for the future.


\begin{figure}[H] 
  \centering 
  \includegraphics[width=0.8\columnwidth]{general}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{General view of the control system}{This is the general control system use on the test benches of the lab before any monitoring or control system was added. A computer with a \Gls{usb} port can be used to send \Gls{json} commands to the \texttt{STM32} control board, with or without the use of Barman's C\# software (both situations were tested during the project). Then the driver board will interpret the \Gls{json} and produce the appropriate \Gls{pwm} on the requested pin, to send to the correct \Gls{led} driver. On this figure, the driver from the Ganzfeld bench is shown, but the system works the same way in the LEDCube, just with a different driver board, for less powerful but more numerous, separated primaries.}{own}\label{fig:general}
\end{figure}

\newpage
\subsection{About the review article}
The analysis article reviewing tenth of other articles with various devices being used to study melanopsin, while being an emergency replacement task amid the global pandemic for my thesis project, turned out to be very useful to understand the challenges of designing a devices for an optical scientific test system.
It gave the opportunity to learn a lot about the way the internal of the human eye works and how scientist are making tests for eye elements not related to the vision, such as the melanopsin.
It also showed the limited amount of information, thus the little importance given to the test devices themselves in some of these research papers.
While the experiment are doing some precise measurement, it seems important to be able to tell the limitations and impact of the testing devices themselves on the experiment.
However, the precise topic of the study of melanopsin using silent substitution method is rather uncommon. Not many people are working on this subject and thus less opportunities existed to try and improve the measuring device setting of these tests. Now, some papers gave plenty of details on the hardware used and even on the way the assembled or designed their test systems\cite{cao_five-primary_2015}\cite{pokorny_photostimulator_2004}\cite{horiguchi_human_2013}. An example of the type of hardware schematic that can be found usually in such articles is visible in the \autoref{fig:pokorny} below.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{pokorny}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Pokorny 2004 optical system}{A schematic in a 2004 article that was reviewed. We can see many details on the optical system and the parts that were used. In the text of the article, some detail was also given on the control electronic for such system.}{Pokorny 2004\cite{pokorny_photostimulator_2004}}\label{fig:pokorny}
\end{figure}