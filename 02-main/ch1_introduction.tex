\chapter{Introduction}\label{chap:introduction}
\minitoc\newpage

% -----------------------------------------------------------------------------
\section{Early project specifications}
\subsection{Goals of the laboratory}\label{sub:goal}
The study subject here in the Nagoya City University's laboratory of Dr.\ Sei-ichi Tsujimura\cite{tsujimura_papers_web} is the relationship between the light stimulation received by a subject's eye and their perception. The general field of study of such topic is called psycophysics, but here the focus is made on the melanopsin: a protein stimulated by light but not involved in the vision. Also a method called ``silent substitution'' is used to test the subject reaction to certain wavelength without stimulating the vision. 

\subsubsection{About melanopsin}
This protein is the research topic for the students here in the lab and also for all the papers reviewed in for the article discussed later, in the \autoref{sec:other_inst}.

The two paragraphs below were written by Mr.\ Martial Geiser (including the references) for the melanopsin devices review article\cite{conus_instrumentation_2020} (details in \autoref{subsec:pandemic}):
\begin{itquote}
  It is today admitted that the eye has five photoreceptors, rods for low light levels, S, M and L cones for color selectivity and some ganglion cells\cite{Lok:2011vg} called intrinsically photosensitive retinal ganglion cell (\textit{ipRGC}, \textit{pRGC}, \textit{pRGC} or simply \textit{melanopsin}). 
  Most retitinal ganglion cells convey contrast and motion information to visual brain centers\cite{hatori_emerging_2010} and approximately $2\%$ of these cells are photosensitive and express melanopsin.

  The melanopsin directly plays a role in circadian rhythms, and to mediate the pupillary light response\cite{McNeill:2011gp}. The temporal properties of \textit{ipRGC} signalling are distinct to those of rods and cones, including longer latencies and sustained signalling after light offset\cite{Joyce:2015hj}.
\end{itquote}

Because this protein and their ganglions are not related to the vision, some alternative methods are used to detect the protein: measuring pupillary responses or shifting in the circadian cycle, depending on the context and the subjects (mice or humans) of the tests.

An important reference was the paper my supervisor,  Dr.\ Tsujimura, wrote with many other people.\cite{brown_melanopsin-based_2012} This document was a starting point for me to understand the wide topic of melanopsin studies and was sent to me before the project started.
This paper presents the context for the study of the melanopsin for both humans and mice. Some general information about the experiments being done on both type of subject are described here and have a good first insight on the way the melanopsin is studied.

\newpage
\subsubsection{About silent substitution}\label{subsub:silent}
A way to test or stimulate the melanopsin, but not the other parts of the eye related to the vision is to use a technique called ``silent substitution''.

Here is a quote of how this work, once again for the article\cite{conus_instrumentation_2020} written with Mr.\ Geiser:
\begin{itquote}
  Silent substitution\cite{cao_five-primary_2015,tsujimura_contribution_2010,estevez_silent_1982} is a technique to excite photoreceptors of the eye in a way that only one photoreceptor is modulated, the others remaining at the same level of excitation (silent). In order to provide such photo excitation, at least four different colors are necessary for photopic excitation or five for mesopic excitation. It is assumed that melanopsin does not respond at scotopic level.
\end{itquote}

\subsection{The project}
The test systems used in the laboratory are required to produce visual stimulation for the eye of test subject in order to study the production of melanopsine. The visual stimulation can be a picture for a video projector, a uniform light covering completely the field of view of the subject (Ganzfeld systems: \autoref{sub:ganzfeld}), or a diffused light, similar to a lamp, that illuminates the environment (LEDCube: \autoref{sub:ledcube}).

\subsection{My job in the laboratory}
This project focuses on a device used in an academic context for experiment on the physiology of the eye. This implies that the system must be reliable enough so the person conducting the experiment can trust it; and that it will be used and potentially modified by non-engineer students.

Before the project started, it was not possible to have access to the devices I will work on (Ganzfeld \& LEDCube), so some early discussion and definition of the project were made remotely. A general specification was given when the project started, but then we needed to have more details about the tasks I should work on.

Eventually it was decided that the first part of the project shall be dedicated to the analysis on the current test systems used in the laboratory and to the design of a solution to improve these.

This initial discussion merged on two topics and on which one of these should be chosen:
\begin{itemize}
\item Using a \Gls{fpga} system to build a fast video system to drive projectors.
\item Using embedded systems technologies to improve the current control system for the \Gls{led}.
\end{itemize}

Both way for the project were considered, yet the first appeared to be both not completely relevant to by Master subject (or at least what I personally wanted to work on) and probably very complex, if not impossible to put into place in its current state. A few hours during the start of the project were dedicated to the study of this, but it turns out that it would require some heavy reverse-engineering or else, some complete redefinition of the way to use the current devices.

While analyzing the situation and the system used in the current test benches in the lab (other that the video projectors), it became clear that the system used to control the \Gls{led} can be improved. Thus this was the chosen direction for the current project. The goal from now on is to add features to the laboratory testing system to have a decent monitoring system will provide a feedback on the control of the device.

In particular, by monitoring the brightness produced by the \Gls{led}s or its temperature, it become possible to improve the quality of the scientific experiments. Having these data allows the experimenter to take them into account for their data analysis and ultimately having more confident in the experiment measurements.

But this applies to every measurement that can be made: the more data we have, the more correlation can be found and thus, the more improved the quality of the experiment by subtracting them. Here are the general topics and goals that were defined from there on the implementation of this monitoring solution:

\begin{itemize}
\item Use an external device to check the current system. This is really the base of it: having a functional monitoring system that we can build on and eventually scale up.
\item Monitor values from the \Gls{led} control system before the \Gls{led}s (current, \Gls{pwm}) and values after the \Gls{led}s (brightness, temperature).
\item Making the measured data readily accessible, both as a live view of the system and also as a log journal that can be used later for deeper analysis of the behavior of the system.
\item Implement a system that is flexible, easy to access, use, deploy and modify. Working hard on a system during a thesis project is good, but if no one is able to use it in a meaningful manner afterward, it becomes useless. This is especially true here as I am an exchange student, providing help and support remotely once the project is finished will be complicated.
\end{itemize}


% -----------------------------------------------------------------------------
\newpage
\section{Structure of this report}
The base structure of this report is split between the following chapters:

\begin{enumerate}
\item \textbf{Introduction:} it is the current chapter.
\item \textbf{Analysis:} having a clear view of the current situation of the systems of the lab and analyse what direction should be taken for the development.
\item \textbf{Design:} looking at the actual way this project system will exist and choosing the best way to archive that.
\item \textbf{Implementation:} creation and deployment of this projects system.
\item \textbf{Validation:} testing that everything goes as expected and according to the objectives.
\item \textbf{Conclusion:} final words and round up of the work done.
\end{enumerate}

The \LaTeX~sources for this document are in their dedicated repository\cite{gitlab_report}.\\
In addition to the details presented in this paper and formatted as above, there are also two ``extra'' documents that are important and complementary to this report:

\subsection{Analysis paper on the existing melanopsin study systems\cite{conus_instrumentation_2020}}
During the first half of the project, a paper aiming to be publish in the peer-review journal MDPI\cite{mdpi_web} was written with Mr.\ Geiser.
It describes the system used in previous papers to study the melanopsin and focuses on the details said articles were providing about their devices.
It makes a detailed introduction for the  choices that were made for the monitoring system developed in this thesis.

Details on the article itself in the \autoref{sec:other_inst}, and the \autoref{subsec:pandemic} talks about the context and the reasons of the creation of this article.

This document is also available in the \autoref{chap:instrumentation_article} of this report.
  
\subsection{GitLab repository wiki pages\cite{gitlab_monitoring_wiki}}
Since the whole structure of the monitoring system has many parts and abstraction layers, a dedicated guide was created in the form of a git wiki for the used to be able to operate and modify the system without have to go through the long reading of present report.
Also because of the existence of this wiki page, less information is available in the individual \texttt{README} pages of the repositories of this project\cite{gitlab_monitoring} or for the other repositories of the GitLab group\cite{gitlab_group} created for this thesis project.

Note that this wiki pages are written using \texttt{org-mode} instead of the traditional \texttt{Markdown}. The two of these are similar, but being a Emacs user, the former is more convenient to use locally.

\subsection{\Gls{cicd} for \LaTeX}\label{sub:latexcicd}
Another element that can be noted about the current report is the use of a continuous integration pipeline to build it, carrying all the dependencies and eventually used to detect building errors. Using a Ubuntu-based Docker container with all the needed \LaTeX~dependencies inside, it is possible to have GitLab automatically building the document every time a push is made.

While being a good training to use Docker and \Gls{cicd} systems, this way to compile a \LaTeX~report gives the advantage to be sure that if a code that does not build was pushed, you will receive a email notification right away. This allow to directly correct the error and avoid having to deal with it later or eventually after some more modifications were made on top of the commit that made the pipeline crash.

You can see the build of this pipeline and get the artifact PDF document in the \Gls{cicd} tab of the repository\cite{gitlab_report_pipeline}.

The \autoref{fig:latex_pipeline} below shows some builds of the report, including one that generated an error.

\begin{figure}[H] 
  \centering 
  \includegraphics[width=1\columnwidth]{latex_pipeline}
  % {TOC entry}{Actual caption}{Source}
  \captionsource{Pipelined automated report builds}{Some pipeline builds of the report, including one that failed. \\
    When such event happened, a email is send to my address so it is possible to see the log of the pipeline build to find what went wrong and correct it.
    The commit that triggered the build is visible, which makes it easier to find the modification that caused an error.
    In this case, the error was an referenced image file that was not pushed to the repositories because of the $.gitignore$ file.\\
    For each of this build, it is possible to get the produced PDF document: this system produces a downloadable PDF version of the report (artifact), accessible from the web interface.
    While the PDF build locally is not pushed with the other files, this is a way for someone to have the latest version of the report without the need to build it or ask me to send it.}{own screenshot}\label{fig:latex_pipeline}
\end{figure}